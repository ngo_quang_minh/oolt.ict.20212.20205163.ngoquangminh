package hust.soict.globalict.aims;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.management.loading.PrivateClassLoader;
import javax.sound.midi.Soundbank;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;

public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}

	
	public static Book getBook() {
		Scanner sc1 = new Scanner(System.in);
		int id;
    	String title;
    	String category;
    	float cost;
    	int nbauthors;
    	List<String>authors = new ArrayList<String>(); 
    	
		System.out.println("Please enter title: ");
		title = sc1.next();
		System.out.println("Please enter category: ");
		category = sc1.next();
		System.out.println("Please enter id: ");
		id = sc1.nextInt();
		
		System.out.println("Please enter number of authors: ");
		nbauthors = sc1.nextInt();
		System.out.println("Enter authors' name:");
		for (int i = 0; i < nbauthors; i++) {
			authors.add(sc1.next());
		}
		System.out.println("Please enter cost: ");
		cost = sc1.nextFloat();
		sc1.close();
		return new Book(title, category, cost, id, authors);
	}
	
    public static void main(String[] args) throws Exception {
    	Order anOrder = new Order();
        Scanner sc = new Scanner(System.in);
    	int choice;
    	int id;
    	
    	MemoryDaemon mDaemon = new MemoryDaemon();
    	Thread thread = new Thread(mDaemon);
        while (true) {
        	showMenu();
        	choice = sc.nextInt();
        	switch (choice) {
        		case 0:
        			sc.close();
        			return;
				case 1: 
					System.out.println("Successfully created new Order");
					break;
				case 2:
					
					anOrder.addMedia(getBook());
					break;
				case 3:
					System.out.println("Please enter the id yout want to delete: ");
					id = sc.nextInt();
					anOrder.removeMedia(anOrder.seachById(id));
					break;
				case 4:
					anOrder.displayOrder();
					break;
				default:
					throw new IllegalArgumentException("Unexpected value: " + choice);
			}
        }
    }
}
