package hust.soict.globalict.aims.utils;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MyDate {
    private int day;
    private int month;
    private int year;

    private final String[] arrMonth = { "", "January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December" };

    private final String[] suffixesDay = { "0th", "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th",
            "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th", "20th", "21st", "22nd", "23rd",
            "24th", "25th", "26th", "27th", "28th", "29th", "30th", "31st" };

    private final String[] arrDay = { "", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth",
            "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth",
            "seventeenth", "eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second", "twenty-third",
            "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth", "twenty-ninth",
            "thirtieth", "thirty-first" };

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate() {
        LocalDate now = LocalDate.now();
        this.year = now.getYear();
        this.month = now.getMonthValue();
        this.day = now.getDayOfMonth();
    }

    public MyDate(String date) {

        String[] arrStr = date.split(" ");

        for (int i = 0; i < arrMonth.length; i++) {
            if (arrStr[0].equals(arrMonth[i]) == true) {
                setMonth(i + 1);
                break;
            }
        }

        setDay(Integer.parseInt(arrStr[1].substring(0, 2)));

        setYear(Integer.parseInt(arrStr[2]));

    }

    public MyDate(String day, String month, String year) {
        for (int i = 1; i < arrDay.length; i++) {
            if (arrDay[i].equals(day)) {
                this.day = i;
                break;
            }

        }
        for (int i = 1; i < arrMonth.length; i++) {
            if (arrMonth[i].equals(month)) {
                this.month = i;
                break;
            }

        }

        this.year = Integer.parseInt(year);
    }

    private boolean check(int year, int month, int day) {
        try {
            LocalDate.of(year, month, day);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (check(year, month, day) == false) {
            System.out.println("INVALID INPUT");
            return;
        }
        this.day = day;

    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (check(year, month, day) == false) {
            System.out.println("INVALID INPUT");
            return;
        }
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (check(year, month, day) == false) {
            System.out.println("INVALID INPUT");
            return;
        }
        this.year = year;
    }

    public void print() {
        System.out.printf("%s %s %d\n", arrMonth[month], suffixesDay[day], year);
    }

    public void printFormatted() {
        String format;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the custom format:");
        format = sc.nextLine();

        LocalDate date = LocalDate.of(year, month, day);
        String formattedDate = date.format(DateTimeFormatter.ofPattern(format));

        System.out.println(formattedDate);
        sc.close();

    }

    public void accept() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the date: ");
        String date = sc.nextLine();

        String[] arrStr = date.split(" ");

        for (int i = 0; i < arrMonth.length; i++) {
            if (arrStr[0].equals(arrMonth[i]) == true) {
                setMonth(i + 1);
                break;
            }
        }

        setDay(Integer.parseInt(arrStr[1].substring(0, 2)));

        setYear(Integer.parseInt(arrStr[2]));

        sc.close();
    }
}