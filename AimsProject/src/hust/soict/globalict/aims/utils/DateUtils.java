package hust.soict.globalict.aims.utils;
public class DateUtils {

    static MyDate tmp;

    public static int compareDate(MyDate date1, MyDate date2) {
        if (date1.getYear() != date2.getYear())
            return (date1.getYear() - date2.getYear());

        if (date1.getMonth() != date2.getMonth())
            return (date1.getMonth() - date2.getMonth());

        if (date1.getDay() != date2.getDay())
            return (date1.getDay() - date2.getDay());

        return 0;
    }

    public static void swap(MyDate d1, MyDate d2) {
        MyDate tmp = new MyDate();

        tmp.setDay(d1.getDay());
        tmp.setMonth(d1.getMonth());
        tmp.setYear(d1.getYear());

        d1.setDay(d2.getDay());
        d1.setMonth(d2.getMonth());
        d1.setYear(d2.getYear());

        d2.setDay(tmp.getDay());
        d2.setMonth(tmp.getMonth());
        d2.setYear(tmp.getYear());
    }

    public static void sortDate(MyDate... dates) {
        for (int i = 0; i < dates.length - 1; i++)
            for (int j = i + 1; j < dates.length; j++) {
                if (compareDate(dates[i], dates[j]) > 0) {
                    swap(dates[i], dates[j]);
                }
            }
    }
}

class coverDate {
    MyDate d;

    coverDate(MyDate d) {
        this.d = d;
    }
}