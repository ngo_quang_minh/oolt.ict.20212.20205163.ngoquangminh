package hust.soict.globalict.aims.order;
import java.util.ArrayList;

import javax.sound.midi.Soundbank;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	
    public static final int MAX_NUMBERS_ORDERED = 10;
    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    private int qtyOdered;
    private MyDate dateOrdered;
    private static int nbOrders = 0;
    public static final int MAX_LIMITTED_ORDERS = 5;

    public Order() {
        if (nbOrders == MAX_LIMITTED_ORDERS) {
            System.out.println("Out of allowed orders!");
            return;
        }
        nbOrders++;
        dateOrdered = new MyDate();
    }

    public int getQtyOdered() {
        return qtyOdered;
    }

    public void setQtyOdered(int qtyOdered) {
        this.qtyOdered = qtyOdered;
    }

    public float totalCost() {
        float sum = 0;

        for (int i = 0; i < itemsOrdered.size(); i++) {
            sum += itemsOrdered.get(i).getCost();
        }

        return sum;
    }

    public void displayOrder() {
        System.out.println("***********************Order***********************");
        System.out.print("Date: ");
        dateOrdered.print();
        System.out.println("Ordered items:");

        for (int i = 0; i < itemsOrdered.size(); i++) {
            System.out.println((i + 1) + " " + itemsOrdered.get(i).getTitle() + " - " + itemsOrdered.get(i).getCategory()+ ": " + itemsOrdered.get(i).getCost() + "$");
        }
        System.out.printf("Total cost: %.2f$\n", totalCost());
        System.out.println("***************************************************");
    }

//    public Media getALuckyItem() {
//        return itemsOrdered.get((int)(Math.random()*qtyOdered));
//    
    public void addMedia(Media obj) {
    	itemsOrdered.add(obj);
		
	}
    
    public void removeMedia(Media obj) {
		itemsOrdered.remove(obj);
	}
    
    public Media seachById(int id) {
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if (itemsOrdered.get(i).getId() == id) return itemsOrdered.get(i);
		}
		
		System.out.println("Cannot found id!");
		return null;
	}
}
