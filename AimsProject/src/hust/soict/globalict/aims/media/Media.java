package hust.soict.globalict.aims.media;

public abstract class Media {

	protected String title;
	protected String category;
	protected float cost;
	protected int id;
	
	
	
	public Media(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}

	public Media(String title) {
		super();
		this.title = title;
	}

	

	public int getId() {
		return id;
	}

	public Media(String title, String category, int id) {
		super();
		this.title = title;
		this.category = category;
		this.id = id;
	}
	
	

	public Media(String title, String category, float cost, int id) {
		super();
		this.title = title;
		this.category = category;
		this.cost = cost;
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public String getCategory() {
		return category;
	}

	public float getCost() {
		return cost;
	}

	public Media() {
		// TODO Auto-generated constructor stub
	}

}
