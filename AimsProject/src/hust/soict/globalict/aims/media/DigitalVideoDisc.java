package hust.soict.globalict.aims.media;
public class DigitalVideoDisc extends Dics implements Playable {
	
    private String director;
    private int length;
    
    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super();
        this.title = title;
        this.category = category;
        this.director = director;
        this.length = length;
        this.cost = cost;
    }

    public DigitalVideoDisc(String title, String category, String director) {
        super();
        this.title = title;
        this.category = category;
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category) {
        super();
        this.title = title;
        this.category = category;
    }

    public DigitalVideoDisc(String title) {
        super();
        this.title = title;
    }

    public DigitalVideoDisc() {
		super();
	}

	public boolean search(String title) {

        String tlow = this.title.toLowerCase();
        title = title.toLowerCase();

        String titleList[] = title.split(" ");

        for (int i = 0; i < titleList.length; i++) {
            if (tlow.contains(titleList[i]) == false)
                return false;
        }

        return true;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }
    
    @Override
    public void play() {
    	// TODO Auto-generated method stub
    	System.out.println("Playing DVD: " + this.getTitle());
    	System.out.println("DVD length: " + this.getLength());
    }

}
