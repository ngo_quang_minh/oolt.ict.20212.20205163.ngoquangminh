package hust.soict.globalict.aims.media;

public class Dics extends Media{
	protected int length;
	protected String dicrector;
	public Dics() {
		super();
	}
	
	public Dics(String title, String category, int length) {
		super(title, category);
		this.length = length;
	}
	
	public Dics(String title, String category, String dicrector) {
		super(title, category);
		this.dicrector = dicrector;
	}
	
	public Dics(String title, String category, int length, String dicrector) {
		super(title, category);
		this.length = length;
		this.dicrector = dicrector;
	}
	
	public int getLength() {
		return length;
	}
	public String getDicrector() {
		return dicrector;
	}

}
