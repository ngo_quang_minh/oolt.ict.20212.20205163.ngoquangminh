package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{
	private List<String>authors = new ArrayList<String>(); 
	

	public Book(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}


	public Book(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}


	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}

	public Book(String title, String category, float cost, int id, List<String> authors) {
		super(title, category, cost, id);
		this.authors = authors;
		// TODO Auto-generated constructor stub
	}


	public List<String> getAuthor() {
		return authors;
	}


	public Book() {
		super();
	}


	public void setAuthor(List<String> author) {
		this.authors = author;
	}
	
	public void addAuthor(String authorName) {
		if (authors.contains(authorName) == false) {
			authors.add(authorName);
		}else {
			System.out.println("The author is already in the list");
		}
	}
	
	public void removeAuthor(String authorName) {
		if (authors.contains(authorName) == true) {
			authors.remove(authorName);
		}else {
			System.out.println("Cannot found the author in the list");
		}
	}
}
