package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDics extends Dics implements Playable {
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	public CompactDics() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CompactDics(String artist, ArrayList<Track> tracks) {
		super();
		this.artist = artist;
		this.tracks = tracks;
	}
	
	public CompactDics(String artist) {
		super();
		this.artist = artist;
	}
	
	public CompactDics(ArrayList<Track> tracks) {
		super();
		this.tracks = tracks;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public void addTrack(Track t) {
		if (tracks.contains(t) == false)
			tracks.add(t);
		else {
			System.out.println("The input track is already in the list ");
		}
	}
	public void removeTrack(Track t) {
		if (tracks.contains(t) == true)
			tracks.remove(t);
		else {
			System.out.println("The input track is not in the list ");
		}
	}
	
	public int getLength(){
		int sum = 0;
		for (int i = 0; i < tracks.size(); i++) {
			sum += tracks.get(i).getLength();
		}
		
		return sum;
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		for (int i = 0; i< tracks.size(); i++) {
			tracks.get(i).play();
		}
	}
}
