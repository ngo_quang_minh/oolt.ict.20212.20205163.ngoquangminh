package hust.soict.globalict.test.utils;
import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
    public static void main(String[] args) {
        MyDate d1 = new MyDate(19, 5, 2022);
        MyDate d2 = new MyDate(23, 4, 2022);
        MyDate d3 = new MyDate(19, 5, 2021);

        // if (DateUtils.compareDate(d1, d2) > 0) {
        // System.out.println("d1 > d2");
        // } else {
        // System.out.println("d1 < d2");
        // }

        // if (DateUtils.compareDate(d2, d3) > 0) {
        // System.out.println("d2 > d3");
        // } else {
        // System.out.println("d2 < d3");
        // }

        d1.print();
        d2.print();
        d3.print();

        DateUtils.sortDate(d1, d2, d3);
        // coverDate cd1 = new coverDate(d1);
        // coverDate cd3 = new coverDate(d3);
        // DateUtils.swap(cd1, cd3);
        // d1.print();
        // d3.print();
        d1.print();
        d2.print();
        d3.print();

    }
}
