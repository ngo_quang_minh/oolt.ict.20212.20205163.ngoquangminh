package hust.soict.globalict.lab01;

import java.util.Scanner;

public class Ex6 {
    public static void firstDegreeOne(){

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a and b (in ax + b = 0)");

        double a = sc.nextDouble();
        if (a == 0){
            System.out.println("Invalid input!");
            sc.close();
            return;
        }
        double b = sc.nextDouble();
        //sc.nextLine();
        //sc.nextLine();
        double x = -b/a;
        System.out.println("Solution for first degree equation: " + x);

        sc.close();

    }

    public static double determinant(double a, double b, double c, double d){
        return a * d - b * c;
    }

    public static void firstDegreeTwo(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the a11, a1, b1 (in a11x1 + a12x2 = b1)");
        double a11 = sc.nextDouble();
        double a12 = sc.nextDouble();
        double b1 = sc.nextDouble();
        System.out.println("Enter the a21, a22, b2 (in a21x1 + a22x2 = b2)");
        double a21 = sc.nextDouble();
        double a22 = sc.nextDouble();
        double b2 = sc.nextDouble();

        double d = determinant(a11, a12, a21, a22);
        double d1 = determinant(b1, a12, b2, a22);
        double d2 = determinant(a11, b1, a21, b2);

        if (d != 0){
            System.out.println("System has a unique solution: " + "(" + (d1/d) +", " + (d2/d) + ")");
        }else{
            if (d1 == 0 && d2 ==0){
                System.out.println("System has infinite solutions!");
            }else{
                System.out.println("System has no solution!");
            }
        }
        sc.close();
    }

    public static double delta(double a, double b, double c){
        return b*b - 4*a*c;
    }

    public static void secondDegree(){
        Scanner sc = new Scanner(System.in);
        double a, b, c;
        System.out.println("Enter the a, b, c (in ax^2 + bx + c = 0)");
        a = sc.nextDouble();
        if (a == 0){
            System.out.println("Invalid input!");
            sc.close();
            return;
        }
        b = sc.nextDouble();
        c = sc.nextDouble();
        double d = delta(a, b, c);
        
        if (d == 0){
            System.out.println("Equation has only one solution: " + (-b/(2*a)));
        }else{
            if (d > 0){
                System.out.println("Equation has two solution: " + (-b + Math.sqrt(d))/(2*a) + " and " + (-b - Math.sqrt(d))/(2*a));
            }else{
                System.out.println("Equation has no solution!");
            }
        }
        sc.close();
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        
            System.out.println("Please enter your order:");
            System.out.println("1 for First degree equation with one variable");
            System.out.println("2 for First degree system with two variables");
            System.out.println("3 for Second degree equation with one variable");

            
            int choice = sc.nextInt();
            switch (choice){
                case 1:
                    firstDegreeOne();
                    sc.nextLine();
                    //sc.nextLine();
                    break;
                case 2:
                    firstDegreeTwo();
                    break;
                case 3:
                    secondDegree();
                    break;
                default:
                    System.out.println("Invalid choice!");
            }
        sc.close();
        
        
    }
}
