package hust.soict.globalict.lab01;

import java.util.Scanner;

public class Ex5{
    public static void main(String[] args) {
        
        double num1, num2, sum, difference, product, quotient;
        Scanner sc = new Scanner(System.in);
        num1 = sc.nextDouble();
        num2 = sc.nextDouble();

        sum = num1 + num2;
        System.out.println("Sum: " + sum);
        difference = Math.abs(num1 - num2);
        System.out.println("Difference: " + difference);

        product = num1 * num2;
        System.out.println("Product: " + product);

        if (num2 == 0){
            System.out.println("Cannot compute with divisor equals to 0");
        }else{
            quotient = num1 / num2;

            System.out.println("Quotient: " + quotient);
        }
        sc.close();
    }
}