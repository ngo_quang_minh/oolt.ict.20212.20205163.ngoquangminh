package hust.soict.globalict.lab02;
import java.util.Scanner;
public class Stars {
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the height n = ");
        int n = sc.nextInt();

        char[][] a = new char[n][2*n];
        
        for (int i = 0; i < n; i++){
            for (int j = n - i -1; j <= n + i - 1; j++) a[i][j] = '*';
        }

        for (int i = 0; i < n; i++){
            for (int j = 0; j < 2*n - 1; j++ )
                if (a[i][j] != '*') System.out.print("  ");
                else System.out.print("* ");
            System.out.println();
        }

        sc.close();
    }
    
}
