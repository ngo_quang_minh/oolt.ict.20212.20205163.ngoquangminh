package hust.soict.globalict.lab02;
import java.util.Scanner;

public class Matrix {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int row, column;
        System.out.println("---This program's purpose is to add two matrices---");
        System.out.print("Enter the row-size of the matrices: ");
        row = sc.nextInt();

        System.out.print("Enter the solumn-size of the matrices: ");
        column = sc.nextInt();

        int[][] a = new int[row][column];
        int[][] b = new int[row][column];

        System.out.println("Enter the first matrix: ");

        for (int i = 0; i < row; i++){
            for (int j = 0; j < column; j++){
                a[i][j] =sc.nextInt();
            }
        }

        System.out.println("Enter the second matrix: ");

        for (int i = 0; i < row; i++){
            for (int j = 0; j < column; j++){
                b[i][j] =sc.nextInt();
            }
        }

        System.out.println("The sum matrix: ");
        for (int i = 0; i < row; i++){
            for (int j = 0; j < column; j++){
                System.out.print(a[i][j] + b[i][j] + " ");
            }
            System.out.println();
        }
    }
}
