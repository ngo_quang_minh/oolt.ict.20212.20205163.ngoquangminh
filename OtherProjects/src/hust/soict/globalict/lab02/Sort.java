package hust.soict.globalict.lab02;

import java.util.Scanner;
public class Sort {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter then number of elements: ");
        int n = sc.nextInt();
        int[] a  = new int [n];
        System.out.println("Enter the elements:");
        for (int i = 0; i<n; i++)
            a[i] = sc.nextInt();
        
        int tmp;
        for (int i = 0; i < n - 1; i++){
            for (int j = i + 1; j < n; j++)
                if (a[i] > a[j]){
                    tmp = a[i];
                    a[i]= a[j];
                    a[j] = tmp;
                }

        }
        System.out.println("Array after sorting:");
        for (int i = 0; i<n; i++)
            System.out.print(a[i] + " ");
        sc.close();
    }
    
}
