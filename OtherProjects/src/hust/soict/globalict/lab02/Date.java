package hust.soict.globalict.lab02;
import java.util.Scanner;
import java.lang.String;
import javax.lang.model.type.ErrorType;

public class Date {
    public  static boolean checkLeap(int year){
        if (year % 4 != 0){
            return false;
        }
        if (year % 400 == 0){
            return true;
        }
        if (year % 100 == 0){
        	return false;
        }
        return true;
    }

    public static boolean checkYear(int year){
        return (year >= 0);
    }

    public static int checkMoth(String month){

        String[][] a = {
            {"January","February","March","April","May","June","July","August","September","October","November","December"},
            {"Jan.", "Feb." ,"Mar." ,"Apr." ,"May.", "June.", "July." ,"Aug." ,"Sept.","Oct.","Nov.","Dec."},
            {"Jan" ,"Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"},
            {"1" ,"2" ,"3" ,"4" ,"5" ,"6", "7" ,"8" ,"9" ,"10" ,"11" ,"12"},
        };

        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 12; j++ ){
                if (a[i][j].equals(month)) return j;
            }
        }
        
        return -1;

    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int year;
        String month;

        int[] days ={31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        do{
            System.out.print("Enter the year: ");
            year = sc.nextInt();
            if (checkYear(year) == false){
                System.out.println("INVALID YEAR!!");
            }else{
                break;
            }
        }while (true);


        do{
            System.out.print("Enter the  month: ");
            month = sc.next();
            System.out.println(month);
        }while(checkMoth(month) == -1);

        if (checkLeap(year) == true) days[1]++;

        System.out.println("The month " + month + " in the year " + year +" has " + days[checkMoth(month)] + " days!");

        //System.out.println(checkMoth("Jan"));
    }    
}
